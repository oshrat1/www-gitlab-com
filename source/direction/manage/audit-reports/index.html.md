---
layout: markdown_page
title: "Category Direction - Audit Reports"
---

- TOC
{:toc}

## Audit Reports

Thanks for visiting this direction page on Audit Reports in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2301) for this category.

Organizations who operate in regulated industries have an obligation to report on their compliance. This often times manifests as obtaining evidence artifacts such as logs, configurations, access lists, and more. Within GitLab, Audit Reports should be easily accessible and provide the necessary information for an internal or external auditor to review.

## Problem to solve

GitLab is used by people with specific job functions and objectives. Day to day, people want to focus on the work they need to get done as part of their primary job responsibilities. The additional workload that comes with audit reports is burdensome and not something people want to do, particularly if it adds additional hours of work to their plate. Extracting audit reports from GitLab to serve as evidence artifacts should be fast and easy.

* What success looks like: Audit Reports should exist for the major areas of GitLab, such as access, activity, code deploys, and deployment pipelines. These reports should provide a granular level of detail about the use of GitLab that an auditor can use to answer all of their questions pertaining to a specific compliance program.

## Our approach

Comprehensive Audit Reports are necessary to satisfy the needs of an organization managing a compliance program. Towards this end, we'll be working on building reports that set a baseline for each major area of GitLab.

The first four areas of focus will be: access, activity, code deploys, and deployment pipelines.

These reports will evolve over time to ensure they meet the needs of our customers' varying compliance program requirements.

## Maturity

Audit Reports is currently in the **planned** state. GitLab does not currently provide features that allow for easy export of important data that could specifically serve as evidence artifacts for a compliance program.

Advancing Audit Reports to the **viable** state requires simple export features that set a baseline for future reporting. These initial reports could be things like:

* a list of all users with access to GitLab
* a list of all merge requests that have been merged
* a list of all pipelines that have run
* a list of activity (audit events) for a specified date range or other filter criteria

## What's Next & Why

We'll be adding minimally viable reporting features, such as [exporting audit events to csv](https://gitlab.com/gitlab-org/gitlab/issues/1449). This feature will move Audit Reports into the **viable** state and provide a baseline for exporting other important audit data from GitLab.

## How you can help

This vision is a work in progress, and everyone can contribute:

* Please comment and contribute in the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via email, Twitter, or on a video call. If you’re a GitLab user and have direct knowledge of your need for compliance and auditing, we’d especially love to hear from you.
* Join our [Compliance Special Interest Group (SIG)](https://gitlab.com/gitlab-org/ux-research/issues/532) where you have a direct line of communication with the PM for Manage:Compliance
